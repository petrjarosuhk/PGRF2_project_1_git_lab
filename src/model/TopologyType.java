package model;

public enum TopologyType {
    POINTS, LINES, LINES_STRIP,TRIANGLE,TRIANGLE_STRIP, TRIANGLE_WIRE
    //další typy
}
